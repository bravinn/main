package com.bravinn.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the applications database table.
 * 
 */
@Entity
@Table(name="applications")
@NamedQuery(name="Application.findAll", query="SELECT a FROM Application a")
public class Application implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ApplicationPK id;

	@Column(name="applciation_code", nullable=false, length=45)
	private String applciationCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="application_apply_date", nullable=false)
	private Date applicationApplyDate;

	@Column(name="application_description", nullable=false, length=400)
	private String applicationDescription;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="application_expiry_date", nullable=false)
	private Date applicationExpiryDate;

	@Column(name="application_status", nullable=false, length=400)
	private String applicationStatus;

	@Column(name="application_title", nullable=false, length=45)
	private String applicationTitle;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false, insertable=false, updatable=false)
	private User user;

	public Application() {
	}

	public ApplicationPK getId() {
		return this.id;
	}

	public void setId(ApplicationPK id) {
		this.id = id;
	}

	public String getApplciationCode() {
		return this.applciationCode;
	}

	public void setApplciationCode(String applciationCode) {
		this.applciationCode = applciationCode;
	}

	public Date getApplicationApplyDate() {
		return this.applicationApplyDate;
	}

	public void setApplicationApplyDate(Date applicationApplyDate) {
		this.applicationApplyDate = applicationApplyDate;
	}

	public String getApplicationDescription() {
		return this.applicationDescription;
	}

	public void setApplicationDescription(String applicationDescription) {
		this.applicationDescription = applicationDescription;
	}

	public Date getApplicationExpiryDate() {
		return this.applicationExpiryDate;
	}

	public void setApplicationExpiryDate(Date applicationExpiryDate) {
		this.applicationExpiryDate = applicationExpiryDate;
	}

	public String getApplicationStatus() {
		return this.applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getApplicationTitle() {
		return this.applicationTitle;
	}

	public void setApplicationTitle(String applicationTitle) {
		this.applicationTitle = applicationTitle;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}