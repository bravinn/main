package com.bravinn.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the applications database table.
 * 
 */
@Embeddable
public class ApplicationPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="application_id", unique=true, nullable=false)
	private int applicationId;

	@Column(name="user_id", insertable=false, updatable=false, unique=true, nullable=false)
	private int userId;

	public ApplicationPK() {
	}
	public int getApplicationId() {
		return this.applicationId;
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	public int getUserId() {
		return this.userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ApplicationPK)) {
			return false;
		}
		ApplicationPK castOther = (ApplicationPK)other;
		return 
			(this.applicationId == castOther.applicationId)
			&& (this.userId == castOther.userId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.applicationId;
		hash = hash * prime + this.userId;
		
		return hash;
	}
}