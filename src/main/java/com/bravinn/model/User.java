package com.bravinn.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
@JsonInclude(Include.NON_EMPTY)
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="user_id", unique=true, nullable=false)
	private int userId;

	@Column(name="adr_city", length=45)
	private String adrCity;

	@Column(name="adr_country", length=45)
	private String adrCountry;

	@Column(name="adr_postal_code", length=45)
	private String adrPostalCode;

	@Column(name="adr_street", length=45)
	private String adrStreet;

	@Column(length=100)
	private String company;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dob;

	@Column(length=45)
	private String email;

	@Column(length=45)
	private String gender;

	@Column(name="marital_status", length=45)
	private String maritalStatus;

	@Column(length=45)
	private String nationality;

	@Column(length=45)
	private String password;

	@Column(name="phone_no", length=45)
	private String phoneNo;

	@Column(length=45)
	private String ssid;

	@Column(length=45)
	private String username;

	//bi-directional many-to-one association to Application
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private List<Application> applications;

	public User() {
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getAdrCity() {
		return this.adrCity;
	}

	public void setAdrCity(String adrCity) {
		this.adrCity = adrCity;
	}

	public String getAdrCountry() {
		return this.adrCountry;
	}

	public void setAdrCountry(String adrCountry) {
		this.adrCountry = adrCountry;
	}

	public String getAdrPostalCode() {
		return this.adrPostalCode;
	}

	public void setAdrPostalCode(String adrPostalCode) {
		this.adrPostalCode = adrPostalCode;
	}

	public String getAdrStreet() {
		return this.adrStreet;
	}

	public void setAdrStreet(String adrStreet) {
		this.adrStreet = adrStreet;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getSsid() {
		return this.ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Application> getApplications() {
		return this.applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}

	public Application addApplication(Application application) {
		getApplications().add(application);
		application.setUser(this);

		return application;
	}

	public Application removeApplication(Application application) {
		getApplications().remove(application);
		application.setUser(null);

		return application;
	}

}