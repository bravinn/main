/**
 * 
 */
package com.bravinn.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.bravinn.model.User;

/**
 * @author Vajahat Ali
 *
 */
public class UserDAO implements UserDAOInterface<User, Integer> {

	private Session currentSession;
	private Transaction currentTransaction;
	
	
	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}
	
	public void closeCurrentSession() {
		currentSession.close();
	}
	
	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}
	
	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	
	
	public Session getCurrentSession() {
		return currentSession;
	}
	
	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}
	public Transaction getCurrentTrans() {
		return currentTransaction;
	}
	
	public void setCurrentTrans(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}
	
	
	
	
	@Override
	public void persist(User entity) {
		getCurrentSession().save(entity);
		
	}

	@Override
	public void update(User entity) {
		getCurrentSession().update(entity);
		
	}

	@Override
	public User findById(Integer id) {
		User user=getCurrentSession().get(User.class, id);
		return user;
	}

	@Override
	public void delete(User entity) {
		getCurrentSession().delete(entity);		
	}

	@Override
	public List<User> findAll() {
		 final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
		            .createEntityManagerFactory("restapi-em");
	        List users = null;

	        // Create an EntityManager
	        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
	        EntityTransaction transaction = null;

	        try {
	            // Get a transaction
	            transaction = manager.getTransaction();
	            // Begin the transaction
	            transaction.begin();

	            // Get a List of Students
	            users = manager.createQuery("SELECT s FROM User s",
	                    User.class).getResultList();

	            // Commit the transaction
	            transaction.commit();
	        } catch (Exception ex) {
	            // If there are any exceptions, roll back the changes
	            if (transaction != null) {
	                transaction.rollback();
	            }
	            // Print the Exception
	            ex.printStackTrace();
	        } finally {
	            // Close the EntityManager
	            manager.close();
	        }
	        return users;
	    }

	@Override
	public void deleteAll() {
		List<User> users=findAll();
		for(User us:users) {
			delete(us);
		}
		
	}

}
