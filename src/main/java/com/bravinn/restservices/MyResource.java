package com.bravinn.restservices;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bravinn.data.UserDAO;
import com.bravinn.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("/user")
public class MyResource {

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to the
	 * client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@Path("/getallusers")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsers() {

		UserDAO ud=new UserDAO();
		List<User> users= ud.findAll();	
		 if(users == null) {
		        return Response.status(Response.Status.NOT_FOUND).entity("No Users found").build();
         }
		 ObjectMapper objectMapper = new ObjectMapper();
		 
		    String json="";
			try {
				json = objectMapper.writeValueAsString(users);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				   return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

			}
		   return Response.ok(json, MediaType.APPLICATION_JSON).build();
		//return users;
	}

	public static void main(String args[]) {

		
		UserDAO ud=new UserDAO();
		//User user= ud.findById(1);
		List<User> users= ud.findAll();	
		
		//just for inserting sample data
		/*EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("restapi-em");
		EntityManager entitymanager = emfactory.createEntityManager();
	
		for (int i = 1; i <= 100; i++) {
			entitymanager.getTransaction().begin();
			User user = new User();
			 user.setUserId(i);
			user.setUsername("username" + i);
			user.setPassword("pass" + i);
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
			user.setDob(date);
			user.setSsid("ssid"+i);
			entitymanager.persist(user);
			entitymanager.getTransaction().commit();
		}

		entitymanager.close();
		emfactory.close();*/
	}
}
